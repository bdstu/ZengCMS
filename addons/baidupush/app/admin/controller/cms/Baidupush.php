<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 百度推送管理
// +----------------------------------------------------------------------
namespace app\admin\controller\cms;

use think\facade\View;
use app\admin\controller\Base;
use addons\baidupush\library\Push;

class Baidupush extends Base
{
    // 初始化
    protected function initialize()
    {
        // 判断插件是否安装
        $addonInfo = getAddonInfo('baidupush');
        if(!$addonInfo || !$addonInfo['status']){
            $this->error($addonInfo?'插件已禁用！':'插件未安装！');
        }
    }
    public function index()
    {
        $config = getAddonConfig('baidupush');
        View::assign('meta_title', '百度推送');
        View::assign('Config', $config);
        return view();
    }
    /**
     * 熊掌号推送
     */
    public function xiongzhang()
    {
        $action = $this->request->post("action");
        $urls = $this->request->post("urls");
        $urls = explode("\n", $urls);
        $urls = array_unique(array_filter($urls));
        if (!$urls) {
            $this->error("URL列表不能为空");
        }
        $result = false;
        if ($action == 'urls') {
            $type = $this->request->post("type");
            if ($type == 'realtime') {
                $result = Push::init(['type' => 'xiongzhang'])->realtime($urls);
            } else {
                $result = Push::init(['type' => 'xiongzhang'])->history($urls);
            }
        } elseif ($action == 'del') {
            $result = Push::init(['type' => 'xiongzhang'])->delete($urls);
        }
        if ($result) {
            $data = Push::init()->getData();
            $this->success("推送成功", null, $data);
        } else {
            $this->error("推送失败：" . Push::init()->getError());
        }
    }
    /**
     * 百度站长推送
     */
    public function zhanzhang()
    {
        $action = $this->request->post("action");
        $urls = $this->request->post("urls");
        $urls = explode("\n", $urls);
        $urls = array_unique(array_filter($urls));
        if (!$urls) {
            $this->error("URL列表不能为空");
        }
        $result = false;
        if ($action == 'urls') {
            $result = Push::init(['type' => 'zhanzhang'])->realtime($urls);
        } elseif ($action == 'del') {
            $result = Push::init(['type' => 'zhanzhang'])->delete($urls);
        }
        if ($result) {
            $data = Push::init()->getData();
            $this->success("推送成功", null, $data);
        } else {
            $this->error("推送失败：" . Push::init()->getError());
        }
    }
}
