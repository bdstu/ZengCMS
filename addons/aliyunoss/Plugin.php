<?php
namespace addons\aliyunoss;

use think\Addons;
use think\facade\Db;
use \OSS\OssClient;
use \OSS\Core\OssException;
use app\common\annotation\HooksAnotation;
use app\member\service\User as home_user;
use app\admin\model\Attachment as Attachment_Model;

class Plugin extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'aliyunoss',
        'title' => '阿里云oss插件',
        'description' => '阿里云oss插件',
        'status' => 1,
        'author' => 'ZengCMS',
        'require' => '1.0.0',
        'version' => '1.0.0',
        'website' => '',
        'images'=>'addons/aliyunoss/images/aliyunoss.jpg',
        'group'=>'',
        'is_hook'=>1,
    ];
    public $menu = [
        'is_nav' => 0,
    ];
    // 上传管理员ID
    public $admin_id = 0;
    // 上传用户ID
    public $user_id  = 0;
    // 会员组
    public $groupid = 0;
    // 是否后台
    public $isadmin     = 0;
    // 插件配置
    public $config = array();
    // 初始化
    protected function init()
    {
        // 判断是否安装
        if(!isAddonInstall($this->name)){
            return false;
        }
        include_once ADDONS_PATH . '/aliyunoss/SDK/autoload.php';
        $this->isLogin();
        $this->config = getAddonConfig('aliyunoss');
    }
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        $upload_driver = Db::name('config')->where(['name' => 'upload_driver'])->find();
        if (!$upload_driver) {
            $this->error = '未找到【上传驱动】配置';
            return false;
        }
        $options = parse_attr($upload_driver['extra']);
        if (isset($options['aliyunoss'])) {
            $this->error = '已存在名为【aliyunoss】的上传驱动';
            return false;
        }
        $upload_driver['extra'] .= PHP_EOL . 'aliyunoss:阿里云oss';
        $result = Db::name('config')
        ->where(['name' => 'upload_driver'])
        ->update(['extra'=>$upload_driver['extra']]);
        if (false === $result) {
            $this->error = '上传驱动设置失败';
            return false;
        }
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        $upload_driver = Db::name('config')->where(['name' => 'upload_driver'])->find();
        if ($upload_driver) {
            $options = parse_attr($upload_driver['extra']);
            if (isset($options['aliyunoss'])) {
                unset($options['aliyunoss']);
            }
            $options = $this->implode_attr($options);
            $result  = Db::name('config')
            ->where(['name' => 'upload_driver'])
            ->update(['extra' => $options, 'value' => $upload_driver['value'] == 'aliyunoss'?'local':$upload_driver['value']]);
            if (false === $result) {
                $this->error = '上传驱动设置失败';
                return false;
            }
        }
        return true;
    }
    /**
     * 插件使用方法
     * @return bool
     */
    public function enabled()
    {
        return true;
    }
    /**
     * 插件禁用方法
     * @return bool
     */
    public function disabled()
    {
        return true;
    }
    /**
     * name 钩子(行为)名称
     * description 钩子描述
     * type 类型(1:视图,2:控制器)
     * system 是否系统
     * status 状态
     * @HooksAnotation(description="上传附件",type="2")
     * @return [type] [description]
     */
    public function UploadOssAfter($params = [])
    {
        // 判断是否调用阿里云的
        if(sys_config('upload_driver') !== 'aliyunoss'){
            return '';
        }
        $this->init();
        $file   = $params['file'];
        $error_msg = '';
        if ($this->config['AccessKeyID'] == '') {
            $error_msg = '未填写阿里云oss【AccessKeyID】';
        } elseif ($this->config['AccessKeySecret'] == '') {
            $error_msg = '未填写阿里云oss【AccessKeySecret】';
        } elseif ($this->config['Bucket'] == '') {
            $error_msg = '未填写阿里云oss【Bucket】';
        } elseif ($this->config['Endpoint'] == '') {
            $error_msg = '未填写阿里云oss【Endpoint】';
        }
        if ($error_msg != '') {
            return json_encode([
                'code'    => -1,
                'info'    => $error_msg,
                'state'   => $error_msg,   // 兼容百度ueditor
                'error'   => 1,
                'success' => 0,          // 兼容editormd
                'message' => $error_msg, // 兼容editormd和kindeditor
            ],JSON_UNESCAPED_UNICODE);
        }
        $info['name'] = $file->getOriginalName();
        $info['mime'] = $file->getOriginalMime();
        $info['tmp_name'] = $file->getPathName();
        $suffix = strtolower(pathinfo($info['name'], PATHINFO_EXTENSION));
        $suffix = $suffix && preg_match("/^[a-zA-Z0-9]+$/", $suffix) ? $suffix : 'file';
        // 要上传文件的本地路径
        $filePath = $info['tmp_name'];
        // 上传到七牛后保存的文件名
        $file_name = explode('.', $info['name']);
        $ext       = end($file_name);
        $key       = $params['dir'] . '/' . date('Ymd') . '/' . $file->hash('md5') . '.' . $ext;
        try {
			$ossClient = new OssClient($this->config['AccessKeyID'], $this->config['AccessKeySecret'], $this->config['Endpoint']);
            $res = $ossClient->uploadFile($this->config['Bucket'], $key, $filePath);
            // 获取附件信息
            $path = $res['info']['url'];
            // 获取文件类型
            if (strpos($info['mime'], 'image/') !== false) {
                $file_type = 'image';
            } elseif (strpos($info['mime'], 'video/') !== false) {
                $file_type = 'video';
            }else{
                $file_type = 'file';
            }
            $data = [
                'aid'    => $this->admin_id,
                'uid'    => $this->user_id,
                'group_id'    => $params['group_id']??0,
                'file_name'=>basename($path),
                'file_type'=>$file_type,
                'name'   => $info['name'],
                'module' => $params['module'],
                'path'   => $path,
                'thumb'  => '',
                'url'    => '',
                'mime'   => $info['mime'],
                'ext'    => $suffix,
                'size'   => $file->getSize(),
                'md5'    => $file->hash('md5'),
                'sha1'   => $file->hash('sha1'),
                'driver' => 'aliyunoss',
                'status'      => 1,
                'sort'        => 100,
                'create_time' => time(),
                'update_time' => time(),
            ];
            if ($file_add = Attachment_Model::create($data)) {
                // 返回结果
                return json_encode([
                    'code'    => 0,
                    'info'    => $data['name'] . '上传成功',
                    'id'      => $file_add['id'],
                    'path'    => $data['path'],
                    'state'   => "SUCCESS",     // 上传状态，上传成功时必须返回"SUCCESS" 兼容百度
                    'url'     => $data['path'], // 返回的地址 兼容百度ueditor和kindeditor
                    'title'   => $data['name'], // 附件名 兼容百度ueditor
                    'error'   => 0,             // 兼容kindeditor
                    'success' => 1,             // 兼容editormd
                    'message' => $data['name'], // 附件名 兼容editormd和kindeditor
                ],JSON_UNESCAPED_UNICODE);
            } else {
                return json_encode([
                    'code'    => -1,
                    'info'    => '上传成功,写入数据库失败',
                    'state'   => '上传成功,写入数据库失败', //兼容百度ueditor
                    'error'   => 1,                       //兼容kindeditor
                    'success' => 0,                       //兼容editormd
                    'message' => '上传成功,写入数据库失败', //兼容editormd和kindeditor
                ],JSON_UNESCAPED_UNICODE);
            }
		} catch (OssException $e) {
            return json_encode([
                'code'    => -1,
                'info'    => '上传失败:' . $e->getMessage(),
                'state'   => '上传失败:' . $e->getMessage(), //兼容百度
                'error'   => 1,                             //兼容kindeditor
                'success' => 0,                             //兼容editormd
                'message' => '上传失败:' . $e->getMessage(), //兼容editormd和kindeditor
            ]);
        }
    }
    /**
     * name 钩子(行为)名称
     * description 钩子描述
     * type 类型(1:视图,2:控制器)
     * system 是否系统
     * status 状态
     * @HooksAnotation(description="上传本地附件",type="2")
     * @return [type] [description]
     */
    public function UploadOss($params = [])
    {
        // 判断是否调用阿里云的
        if(sys_config('upload_driver') !== 'aliyunoss'){
            return '';
        }
        $this->init();
        $error_msg = '';
        if ($this->config['AccessKeyID'] == '') {
            $error_msg = '未填写阿里云oss【AccessKeyID】';
        } elseif ($this->config['AccessKeySecret'] == '') {
            $error_msg = '未填写阿里云oss【AccessKeySecret】';
        } elseif ($this->config['Bucket'] == '') {
            $error_msg = '未填写阿里云oss【Bucket】';
        } elseif ($this->config['Endpoint'] == '') {
            $error_msg = '未填写阿里云oss【Endpoint】';
        }
        if ($error_msg != '') {
            return json_encode([
                'code'    => -1,
                'info'    => $error_msg,
                'state'   => $error_msg,// 兼容百度ueditor
                'error'   => 1,         // 兼容kindeditor
                'success' => 0,         // 兼容editormd
                'message' => $error_msg,// 兼容editormd和kindeditor
            ],JSON_UNESCAPED_UNICODE);
        }
        $key = str_replace('uploads/', '', $params['path']);
        try {
			$ossClient = new OssClient($this->config['AccessKeyID'], $this->config['AccessKeySecret'], $this->config['Endpoint']);
            $res = $ossClient->uploadFile($this->config['Bucket'], $key, get_file_path($params['path'],2));
            // 获取附件信息
            $path = $res['info']['url'];
            $params['path'] = $path;
            $params['driver'] = 'aliyunoss';
            if ($file_add = Attachment_Model::create($params)) {
                // 返回结果
                return json_encode([
                    'code'    => 0,
                    'info'    => $params['name'] . '上传成功',
                    'id'      => $file_add['id'],
                    'path'    => $path,
                    'state'   => "SUCCESS",      // 上传状态，上传成功时必须返回"SUCCESS" 兼容百度
                    'title'   => $params['name'],// 附件名，兼容百度
                    'url'     => $path,          // 返回的地址，兼容百度和editormd
                    'error'   => 0,              // 兼容kindeditor
                    'success' => 1,              // 兼容editormd
                    'message' => $params['name'],// 附件名，兼容editormd和kindeditor
                ],JSON_UNESCAPED_UNICODE);
            } else {
                return json_encode([
                    'code'    => -1,
                    'info'    => '上传成功,写入数据库失败',
                    'state'   => '上传成功,写入数据库失败', //兼容百度ueditor
                    'error'   => 1,                       //兼容kindeditor
                    'success' => 0,                       //兼容editormd
                    'message' => '上传成功,写入数据库失败', //兼容editormd和kindeditor
                ],JSON_UNESCAPED_UNICODE);
            }
		} catch (OssException $e) {
            return json_encode([
                'code'    => -1,
                'info'    => '上传失败:' . $e->getMessage(),
                'state'   => '上传失败:' . $e->getMessage(), //兼容百度ueditor
                'error'   => 1,                             //兼容kindeditor
                'success' => 0,                             //兼容editormd
                'message' => '上传失败:' . $e->getMessage(), //兼容editormd和kindeditor
            ]);
        }
    }
    /**
     * name 钩子(行为)名称
     * description 钩子描述
     * type 类型(1:视图,2:控制器)
     * system 是否系统
     * status 状态
     * @HooksAnotation(description="删除oss附件",type="2")
     * @return [type] [description]
     */
    public function DeleteOss($params = [])
    {
        // 判断是否调用阿里云的
        if(sys_config('upload_driver') !== 'aliyunoss'){
            return '';
        }
        $this->init();
        $key = str_replace(rtrim('http://'.$this->config['Bucket'].'.'.$this->config['Endpoint'], '/') . '/', '', $params['path']);
        $ossClient = new OssClient($this->config['AccessKeyID'], $this->config['AccessKeySecret'], $this->config['Endpoint']);
        $ossClient->deleteObject($this->config['Bucket'], $key);
        return true;
    }
    protected function isLogin()
    {
        if (is_login() > 0) {
            $this->isadmin  = 1;
            $this->admin_id = is_login();
        } elseif (home_user::instance()->isLogin()) {
            $this->user_id = home_user::instance()->id;
            $this->groupid = home_user::instance()->groupid ? home_user::instance()->groupid : 8;
        } else {
            $this->admin_id = 0;
            $this->user_id = 0;
        }
    }
    protected function implode_attr($array = [])
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[] = $key . ':' . $value;
        }
        return empty($result) ? '' : implode(PHP_EOL, $result);
    }
}
