<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 优化设置控制器
// +----------------------------------------------------------------------
namespace app\admin\controller\cms;

use think\facade\Db;
use think\facade\View;
use think\facade\Request;
use think\addons\Service;
use app\admin\controller\Base;
use app\common\annotation\NodeAnotation;
use app\common\annotation\ControllerAnnotation;
/**
 * @ControllerAnnotation(title="优化设置管理")
 * Class Seoset
 * @package app\admin\controller\cms
 */
class Seoset extends Base
{
    /**
     * @NodeAnotation(title="首页优化")
     */
    public function site()
    {
        $map[] = ['name', 'in', 'WEB_SITE_TITLE,WEB_SITE_TITLE_EN,WEB_SITE_KEYWORDS,WEB_SITE_KEYWORDS_EN,WEB_SITE_DESCRIPTION,WEB_SITE_DESCRIPTION_EN'];
        $map[] = ['status','=',1];
        $list = Db::name('config')->where($map)->order('sort desc')->field('id,title,name,value,extra,type,remark')->select()->toArray();
        // dump($list);die;
        // 记录当前列表页的cookie
        cookie('__forward__', $_SERVER['REQUEST_URI']);
        View::assign([
            'meta_title' => '首页优化',
            'list' => $list,
        ]);
        return view();
    }
    /**
     * @NodeAnotation(title="栏目优化")
     */
    public function category()
    {
        $tree = Db::name('arctype')->field('id,pid,typename,name')->order('sort asc,id asc')->select()->toArray();
        $tree = cate_level($tree);//栏目层级归类
        cookie('__forward__', $_SERVER['REQUEST_URI']);
        View::assign([
            'meta_title' => '栏目优化',
            'list' => $tree,
        ]);
        return view();
    }
    /**
     * @NodeAnotation(title="编辑栏目")
     */
    public function editCategory($id = null)
    {
        $Category = Db::name('arctype');
        if (request()->isPost()) { //提交表单
            $data = input('post.');
            if (false !== $Category->where('id', $data['id'])->strict(false)->update($data)) {
                $this->success('编辑成功！', cookie('__forward__'));
            } else {
                $this->error('编辑失败！');
            }
        } else {
            if (empty($id)) {
                $this->error('参数错误！', cookie('__forward__'));
            }
            // 获取分类信息
            $info = $id ? $Category->find($id) : '';
            View::assign([
                'meta_title' => '编辑【' . $info['typename'] . '】栏目',
                'info' => $info,
            ]);
            return view();
        }
    }
    /**
     * @NodeAnotation(title="地区优化")
     */
    public function area()
    {
        if (request()->isPost()) {
            /* // 更新优化城市
            $map[] = ['name', '=', 'WEB_SEO_CITY'];
            $data['value'] = json_encode(input('area'));
            $flag = Db::name('config')->where($map)->update($data);

            // 更新优化栏目
            $map2[] = ['name', '=', 'WEB_SEO_MENU'];
            $data2['value'] = json_encode(input('seomenu'));
            $flag2 = Db::name('config')->where($map2)->update($data2);
            if ($flag || $flag2) {
                $this->success('更新成功！', url('area'));
            } else {
                $this->error('更新失败！', url('area'));
            } */
            $params['WEB_SEO_CITY'] = json_encode(input('area'));
            $params['WEB_SEO_MENU'] = json_encode(input('seomenu'));
            $info =  Db::name('addon')->where('name','cms')->find();
            if ($params) {
                $config = @unserialize($info['config']);
                foreach ($config as $k => &$v) {
                    if (isset($params[$k])) {
                        if ($v['type'] == 'array') {
                            $arr = [];
                            $params[$k] = is_array($params[$k]) ? $params[$k] :unserialize($params[$k]);
                            foreach($params[$k][0] as $k2=>$v2){
                                if($v2){
                                    $arr[$v2] = $params[$k][1][$k2];
                                }
                            }
                            $params[$k] = $arr;
                            $value = $params[$k];
                            $v['content'] = $value;
                        } else {
                            $value = is_array($params[$k]) ? serialize($params[$k]) : $params[$k];
                        }
                        $v['value'] = $value;
                    }
                }
                $config = serialize($config);
                $res = Db::name('addon')->where('name','cms')->update(['config'=>$config,'update_time'=>time()]);
                if($res !== false){
                    Service::updateAdddonsConfig();
                    $this->success(lang('edit success'));
                }else{
                    $this->error(lang('edit fail'));
                }
            }
            $this->error(lang('parameter can not be empty'));
        } else {
            $map[] = ['leveltype', '<', 3];
            // 获取城市信息
            $area = Db::name('region')->where($map)->field('id,name,shortname')->select()->toArray();
            unset($area[0]);
            $tmap[] = ['status', '=', 1];
            $tmap[] = ['pid', '=', 0];
            // 获取顶级栏目信息
            $arctype = Db::name("arctype")->field("id,typename")->where($tmap)->order("sort asc,id asc")->select()->toArray();
            // 获取已优化城市
            $WEB_SEO_CITY = get_addon_config('cms','WEB_SEO_CITY');
            // 获取已地区优化栏目
            $WEB_SEO_MENU = json_decode(get_addon_config('cms','WEB_SEO_MENU'));
            if ($WEB_SEO_MENU) {
                $WEB_SEO_MENU = implode(',', $WEB_SEO_MENU);
            }
            View::assign([
                'meta_title' => '地区优化',
                'area' => $area,//所有城市信息
                'arctype' => $arctype,//所有顶级栏目信息
                'WEB_SEO_CITY' => $WEB_SEO_CITY,//获取城市信息
                'WEB_SEO_MENU' => $WEB_SEO_MENU,//获取已地区优化的栏目
            ]);
            return view();
        }
    }
    /**
     * @NodeAnotation(title="长尾词优化")
     */
    public function longKey()
    {
        if (Request::isPost()) {
            $params = $this->request->post('config/a',[],'trim');
            $info =  Db::name('addon')->where('name','cms')->find();
            if ($params) {
                $config = @unserialize($info['config']);
                foreach ($config as $k => &$v) {
                    if (isset($params[$k])) {
                        if ($v['type'] == 'array') {
                            $arr = [];
                            $params[$k] = is_array($params[$k]) ? $params[$k] :unserialize($params[$k]);
                            foreach($params[$k][0] as $k2=>$v2){
                                if($v2){
                                    $arr[$v2] = $params[$k][1][$k2];
                                }
                            }
                            $params[$k] = $arr;
                            $value = $params[$k];
                            $v['content'] = $value;
                        } else {
                            $value = is_array($params[$k]) ? serialize($params[$k]) : $params[$k];
                        }
                        $v['value'] = $value;
                    }
                }
                $config = serialize($config);
                $res = Db::name('addon')->where('name','cms')->update(['config'=>$config,'update_time'=>time()]);
                if($res !== false){
                    Service::updateAdddonsConfig();
                    $this->success(lang('edit success'));
                }else{
                    $this->error(lang('edit fail'));
                }
            }
            $this->error(lang('parameter can not be empty'));
        }
        $info = array();
        foreach(getAddonConfig('cms',true) as $k=>$v){
            if(in_array($k,['WEB_LONGKEY_PREFIX','WEB_LONGKEY_SUFFIX'])){
                $info[$k] = $v;
            }
        }
        View::assign([
            'meta_title' => "长尾词优化",
            'list' => $info,
        ]);
        // 记录当前列表页的cookie
        cookie('__forward__', $_SERVER['REQUEST_URI']);
        return view();
    }
    /**
     * @NodeAnotation(title="优化操作")
     */
    public function operate()
    {
        if (request()->isPost()) {
            $num = null;
            for ($i = 0; $i < count($_POST["id"]); $i++) {
                $data["id"] = intval($_POST["id"][$i]);
                $data["title"] = $_POST["title"][$i];
                $data["name"] = $_POST["name"][$i];
                $data["value"] = $_POST["content"][$i];
                $arr = Db::name('config')->where('id', $data['id'])->update($data);
                if (is_numeric($arr)) {
                    $num += $arr;
                }
            }
            if ($num) {
                $this->success('更新成功！', cookie('__forward__'));
            } else {
                $this->error('内容无更新！', cookie('__forward__'));
            }
        } else {
            $this->error('更新失败！', cookie('__forward__'));
        }
    }
}