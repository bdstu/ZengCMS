<?php
namespace addons\collection;

use think\Addons;

class Plugin extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'collection',
        'title' => '万能采集',
        'description' => '强大而简用的采集神器！',
        'status' => 1,
        'author' => 'ZengCMS',
        'require' => '1.0.0',
        'version' => '1.0.0',
        'website' => '',
        'images'=>'addons/collection/images/collection.jpg',
        'group'=>'',
        'is_hook'=>0,
        // 依赖插件
        'need_plugin' => [
            ['cms','1.0.0','>='],
        ],
        'tables' => [
            'node',
            'html',
        ],
    ];
    public $menu = [
        'is_nav' => 0,
        'menu' => [
            "title" => '采集管理',
            "title_en" => 'Collection node list',
            'name'=>'admin/cms.Collection/node_list',
            'type'=>1,
            'condition'=>'',
            "status" => 1,
            'show'=>1,
            "icon" =>'bug',
            'remark'=>'',
            "sort" => 50,
            'menulist' => [
                [
                    "title" => '添加列表页采集节点配置',
                    'name'=>'admin/cms.Collection/add_list_rules',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '添加内容页采集节点配置',
                    'name'=>'admin/cms.Collection/add_item_rules',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '编辑',
                    'name'=>'admin/cms.Collection/edit',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '状态',
                    'name'=>'admin/cms.Collection/setStatus',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '排序',
                    'name'=>'admin/cms.Collection/sort',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '删除',
                    'name'=>'admin/cms.Collection/del',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '展示采集界面',
                    'name'=>'admin/cms.Collection/show',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '执行采集',
                    'name'=>'admin/cms.Collection/do_caiji',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '导出数据到模型表',
                    'name'=>'admin/cms.Collection/export_data',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '采集数据列表',
                    'name'=>'admin/cms.Collection/data_list',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '查看采集数据',
                    'name'=>'admin/cms.Collection/data_view',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '刪除采集数据',
                    'name'=>'admin/cms.Collection/data_del',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
            ]
        ]
    ];
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }
    /**
     * 插件使用方法
     * @return bool
     */
    public function enabled()
    {
        return true;
    }
    /**
     * 插件禁用方法
     * @return bool
     */
    public function disabled()
    {
        return true;
    }
}
