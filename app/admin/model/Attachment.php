<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 附件上传模型
// +----------------------------------------------------------------------
namespace app\admin\model;

use think\Model;

class Attachment extends Model
{
    // 自动写入时间戳
    protected $autoWriteTimestamp = true;
    public function getSizeAttr($value)
    {
        return format_bytes($value);
    }
    /**
     * 根据附件id获取路径
     * @param  string|array $id 附件id
     * @param  int $type 类型：0-补全目录，1-直接返回数据库记录的地址
     * @return string|array     路径
     */
    public function getFilePath($id = '', $type = 0)
    {
        $isIds = strpos($id, ',') !== false;
        if ($isIds) {
            $ids = explode(',', $id);
            $data_list = $this->where('id', 'in', $ids)
            ->field('path,driver,thumb')
            ->orderField('id', $ids)
            ->select()
            ->toArray();
            $paths = [];
            foreach ($data_list as $key => $value) {
                $paths[$key] = $value['path'];
            }
            return $paths;
        } else {
            $data = $this->where('id', $id)->field('path,driver,thumb')->find();
            if ($data) {
                return $data['path'];
            } else {
                return false;
            }
        }
    }
    /**
     * 根据附件id获取名称
     * @param  string $id 附件id
     * @return string     名称
     */
    public function getFileName($id = '')
    {
        return $this->where('id', $id)->value('name');
    }
    // 删除附件
    public function deleteFile($id)
    {
        $isAdministrator = is_super_administrator(is_login());
        $aid = is_login();
        $attachment = $isAdministrator ? self::where('id', $id)->field('file_name,path,driver')->find() : self::where('id', $id)->where('aid', $aid)->field('file_name,path,driver')->find();
        if($attachment){
            if($attachment['driver'] == 'local'){
                $path = PROJECT_PATH . '/public/static/' . $attachment['path'];
                if (file_exists($path)) {
                    if (!unlink($path)) {
                        throw new \Exception("删除" . $attachment['path'] . "失败");
                    }
                }
            }else{
                $result = hook('DeleteOss', ['file_name'=>$attachment['file_name'],'path'=>$attachment['path']], false);
                if(!$result){
                    throw new \Exception("删除" . $attachment['path'] . "失败");
                }
            }
            self::where('id', $id)->delete();
        } else {
            throw new \Exception($isAdministrator ? "文件数据库记录已不存在~" : "没有权限删除别人上传的附件~");
        }
    }
}
