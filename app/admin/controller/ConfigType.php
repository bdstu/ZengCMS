<?php 
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 配置类型控制器
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\Db;
use think\facade\View;
use app\common\annotation\NodeAnotation;
use app\common\annotation\ControllerAnnotation;
/**
 * @ControllerAnnotation(title="配置类型管理")
 * Class ConfigType
 * @package app\admin\controller
 */
class ConfigType extends Base
{
    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        $map = array();
        $query = array();
        $status = trim(input('status'));
        $title = trim(input('title'));
        if ($status !== '' && ($status == 0 || $status == 1)) {
            $map[] = ['status', '=', $status];
            $query['status'] = $status;
        }
        if ($title) {
            $map[] = ['title', 'like', "%$title%"];
            $query['title'] = $title;
        }
        $list = Db::name('config_type')->where($map)->order(['sort' => 'desc', 'id' => 'asc'])
        ->paginate(['list_rows'=> sys_config('WEB_ONE_PAGE_NUMBER'),'var_page' => 'page','query' => $query])
        ->each(function ($item, $key) {
            int_to_string($item,array('status' => array('0' => '隐藏', '1' => '显示')));
            return $item;
        });
        View::assign([
            'meta_title' => '配置类型列表',
            'status' => $status,
            'list' => $list,
        ]);
        // 记录当前列表页的cookie
        cookie('__forward__', $_SERVER['REQUEST_URI']);
        return view();
    }
    /**
     * @NodeAnotation(title="新增")
     */
    public function add()
    {
        if (request()->isAjax()) {
            $data = input('post.');
            $validate = validate('ConfigType');
            if (!$validate->scene('add')->check($data)) {
                $this->error($validate->getError());
            }
            $data['create_time'] = time();
            $data['update_time'] = time();
            $id = Db::name('config_type')->strict(true)->insertGetId($data);
			if (!$id) {
                $this->error('新增出错！');
			}
			action_log($id, 'config_type', 1);
            $this->success('新增成功！');
        } else {
            View::assign([
                'meta_title' => '新增配置类型',
                'info' => null,
            ]);
            return view('edit');
        }
    }
    /**
     * @NodeAnotation(title="编辑")
     */
    public function edit($id = null)
    {
        if (request()->isAjax()) {
            $data = input('post.');
            $validate = validate('ConfigType');
            if (!$validate->scene('edit')->check($data)) {
                $this->error($validate->getError());
            }
            $data['id'] = intval($data['id']);
			if ($data['id'] == 0) {
                $this->error('非法操作！');
            }
            $data['update_time'] = time();
			action_log($data['id'],'config_type', 2);//记录修改前行为
			$res = Db::name('config_type')->strict(false)->where('id',$data['id'])->update($data);
			if(!$res){
                $this->error('更新失败！');
            }
            action_log($data['id'],'config_type',2);//记录修改后行为
            $this->success('更新成功！',cookie('__forward__'));
        } else {
            if (empty($id)) {
                $this->error('参数错误！');
            }
            $info = Db::name('config_type')->find($id);
            if (!$info) {
                $this->error('信息错误！');
            }
            View::assign([
                'meta_title' => '编辑配置类型',
                'info' => $info,
            ]);
            return view();
        }
    }
    /**
     * @NodeAnotation(title="删除")
     */
    public function del($ids = NULL)
    {
        $ids = !empty($ids) ? $ids : input('ids', 0);
        if (empty($ids)) {
            $this->error('参数不能为空！');
        }
        if (!is_array($ids)) {
            $ids = array(intval($ids));
        }
        if (Db::name('config')->field('id')->where([['typeid', 'in', $ids]])->count()) {
            $this->error('请先删除该配置类型下的所有配置！');
        } else {
            foreach ($ids as $k => $v) {
                action_log($v, 'config_type', 3);
                Db::name('config_type')->delete($v);
            }
            $this->success('删除配置类型成功！');
        }
    }
    /**
     * @NodeAnotation(title="状态")
     */
    public function setStatus($model = 'config_type', $data = array(), $type = 1)
    {
        $ids = input('ids');
        $status = input('status');
        $data['ids'] = $ids;
        $data['status'] = $status;
        return parent::setStatus($model, $data, $type);
    }
    /**
     * @NodeAnotation(title="排序")
     */
    public function sort($model = 'config_type', $data = array())
    {
        $data['sort'] = input('sort');
        return parent::sort($model, $data);
    }
}
