<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 附件上传处理类
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\facade\Db;
use think\facade\Config;
use imagetool\ImageTool;
use think\facade\Filesystem;
use app\common\controller\Base;
use app\member\service\User as home_user;
use app\common\annotation\NodeAnotation;
use app\common\annotation\ControllerAnnotation;
use app\admin\model\Attachment as Attachment_Model;
/**
 * @ControllerAnnotation(title="上传管理")
 * Class Upload
 * @package app\admin\controller
 */
class Upload extends Base
{
    // 上传管理员ID
    public $admin_id    = 0;
    // 上传用户ID
    public $user_id     = 0;
    // 会员组
    public $groupid     = 0;
    // 是否后台
    public $isadmin     = 0;
    // 上传模块
    public $module      = 'cms';
    // 编辑器初始配置
    private $confing = array(
        /* 上传图片配置项 */
        "imageActionName"         => "uploadimage", /* 执行上传图片的action名称 */
        "imageFieldName"          => "upfile", /* 提交的图片表单名称 */
        "imageMaxSize"            => 2048000, /* 上传大小限制，单位B */
        "imageAllowFiles"         => [".png", ".jpg", ".jpeg", ".gif", ".bmp"], /* 上传图片格式显示 */
        "imageCompressEnable"     => true, /* 是否压缩图片,默认是true */
        "imageCompressBorder"     => 1600, /* 图片压缩最长边限制 */
        "imageInsertAlign"        => "none", /* 插入的图片浮动方式 */
        "imageUrlPrefix"          => "", /* 图片访问路径前缀 */
        'imagePathFormat'         => '',
        /* 涂鸦图片上传配置项 */
        "scrawlActionName"        => "uploadscrawl", /* 执行上传涂鸦的action名称 */
        "scrawlFieldName"         => "upfile", /* 提交的图片表单名称 */
        'scrawlPathFormat'        => '',
        "scrawlMaxSize"           => 2048000, /* 上传大小限制，单位B */
        'scrawlUrlPrefix'         => '',
        'scrawlInsertAlign'       => 'none',
        /* 截图工具上传 */
        "snapscreenActionName"    => "uploadimage", /* 执行上传截图的action名称 */
        'snapscreenPathFormat'    => '',
        'snapscreenUrlPrefix'     => '',
        'snapscreenInsertAlign'   => 'none',
        /* 抓取远程图片配置 */
        'catcherLocalDomain'      => array('127.0.0.1', 'localhost', 'img.baidu.com'),
        "catcherActionName"       => "catchimage", /* 执行抓取远程图片的action名称 */
        'catcherFieldName'        => 'source',
        'catcherPathFormat'       => '',
        'catcherUrlPrefix'        => '',
        'catcherMaxSize'          => 0,
        'catcherAllowFiles'       => array('.png', '.jpg', '.jpeg', '.gif', '.bmp'),
        /* 上传视频配置 */
        "videoActionName"         => "uploadvideo", /* 执行上传视频的action名称 */
        "videoFieldName"          => "upfile", /* 提交的视频表单名称 */
        'videoPathFormat'         => '',
        'videoUrlPrefix'          => '',
        'videoMaxSize'            => 0,
        'videoAllowFiles'         => array(".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg", ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid"),
        /* 上传文件配置 */
        "fileActionName"          => "uploadfile", /* controller里,执行上传视频的action名称 */
        'fileFieldName'           => 'upfile',
        'filePathFormat'          => '',
        'fileUrlPrefix'           => '',
        'fileMaxSize'             => 0,
        'fileAllowFiles'          => array(".flv", ".swf"),
        /* 列出指定目录下的图片 */
        "imageManagerActionName"  => "listimage", /* 执行图片管理的action名称 */
        'imageManagerListPath'    => '',
        'imageManagerListSize'    => 20,
        'imageManagerUrlPrefix'   => '',
        'imageManagerInsertAlign' => 'none',
        'imageManagerAllowFiles'  => array('.png', '.jpg', '.jpeg', '.gif', '.bmp'),
        /* 列出指定目录下的文件 */
        "fileManagerActionName"   => "listfile", /* 执行文件管理的action名称 */
        'fileManagerListPath'     => '',
        'fileManagerUrlPrefix'    => '',
        'fileManagerListSize'     => '',
        'fileManagerAllowFiles'   => array(".flv", ".swf"),
    );
    protected function initialize()
    {
        parent::initialize();
        if (is_login() > 0) {
            $this->isadmin = 1;
            $this->admin_id = is_login();
        } elseif (home_user::instance()->isLogin()) {
            $this->user_id = home_user::instance()->id;
            $this->groupid = home_user::instance()->groupid ? home_user::instance()->groupid : 8;
        } else {
            return json([
                'code'    => -1,
                'info'    => '未登录！',
                'state'   => '未登录！',// 兼容百度ueditor
                'error'   => 1,        // 兼容kindeditor
                'success' => 0,        // 兼容editormd
                'message' => '未登录！',// 兼容editormd和kindeditor
            ]);
        }
        // 图片上传大小和类型
        $this->confing['imageMaxSize'] = $this->confing['catcherMaxSize'] = 0 == sys_config('upload_image_size',0) ? 1024 * 1024 * 1024 : sys_config('upload_image_size') * 1024;
        if (!empty(sys_config('upload_image_ext'))) {
            $imageallowext = parse_attr(sys_config('upload_image_ext'));
            foreach ($imageallowext as $k => $rs) {
                $imageallowext[$k] = ".{$rs}";
            }
            $this->confing['imageAllowFiles'] = $imageallowext;
        }
        // 附件上传大小和类型
        $this->confing['fileMaxSize'] = $this->confing['videoMaxSize'] = 0 == sys_config('upload_file_size',0) ? 1024 * 1024 * 1024 : sys_config('upload_file_size') * 1024;
        if (!empty(sys_config('upload_file_ext'))) {
            $fileallowext = parse_attr(sys_config('upload_file_ext'));
            foreach ($fileallowext as $k => $rs) {
                $fileallowext[$k] = ".{$rs}";
            }
            $this->confing['fileAllowFiles'] = $fileallowext;
        }
    }
    /**
     * @NodeAnotation(title="上传")
     */
    public function upload($dir = '', $from = '', $module = '')
    {
        // 验证是否可以上传
        $status = $this->isUpload($module);
        if (true !== $status) {
            return json([
                'code'    => -1,
                'info'    => $status,
                'state'   => $status, // 兼容百度ueditor
                'error'   => 1,       // 兼容kindeditor
                'success' => 0,       // 兼容editormd
                'message' => $status, // 兼容editormd和kindeditor
            ]);
        }
        if ($dir == '') {
            return json([
                'code'    => -1,
                'info'    => '没有指定上传目录！',
                'state'   => '没有指定上传目录！',// 兼容百度ueditor
                'error'   => 1,                 // 兼容kindeditor
                'success' => 0,                 // 兼容editormd
                'message' => '没有指定上传目录！',// 兼容editormd和kindeditor
            ]);
        }
        if ($from == 'ueditor') {
            return $this->ueditor();
        } elseif ($from == 'kindeditor') {
            return $this->kindeditor();
        }elseif($from == 'upload_max_file'){
            return $this->upload_max_file();
        }else{
            return $this->saveFile($dir, $from, $module);
        }
    }
    // 检查是否可以上传
    protected function isUpload($module)
    {
        $module_list['admin'] = [];
        $module_list['cms'] = [];
        $module_list['addons'] = [];
        if ($module_list[strtolower($module)] || strtolower($module) == 'admin' || strtolower($module) == 'addons') {
            $this->module = strtolower($module);
        } else {
            return false;
        }
        // 如果是前台上传，判断用户组权限
        if ($this->isadmin == 0 && $this->user_id != 0) {
            $member_group = $this->get_member_group();
            if ((int) $member_group[$this->groupid]['allowattachment'] < 1) {
                return "所在的用户组没有附件上传权限！";
            }
        }
        return true;
    }
    /**
     * 保存附件
     * @param string $dir 附件存放的目录
     * @param string $from 来源
     * @param string $module 来自哪个模块
     * @return string|\think\response\Json
     */
    protected function saveFile($dir = '', $from = '', $module = '')
    {
        if (!function_exists("finfo_open")) {
            return json([
                'code'  => -1,
                'info'  => '检测到环境未开启php_fileinfo拓展',
                'state' => '检测到环境未开启php_fileinfo拓展',  // 兼容百度ueditor
                'error'   => 1,                               // 兼容kindeditor
                'success' => 0,                               // 兼容editormd
                'message' => '检测到环境未开启php_fileinfo拓展',// 兼容editormd和kindeditor
            ]);
        }
        // 获取附件数据
        switch ($from) {
            case 'editormd':
                $file_input_name = 'editormd-image-file';
                break;
            case 'ueditor':
                $file_input_name = 'upfile';
                break;
            case 'kindeditor':
                $file_input_name = 'imgFile';
                break;
            default:
                $file_input_name = 'file';
        }
        $file = $this->request->file($file_input_name);
        if ($file == null) {
            return json([
                'code'    => -1,
                'info'    => '获取不到文件信息',
                'state'   => '获取不到文件信息', // 兼容百度ueditor
                'error'   => 1,                // 兼容kindeditor
                'success' => 0,                // 兼容editormd
                'message' => '获取不到文件信息',// 兼容editormd和ueditor
            ]);
        }
        // 判断附件是否已存在
        if(($is_exist = $this->is_exist($file->hash('md5'))) !== false){
            if($from == 'kindeditor'){
                $arr = json_decode($is_exist,true);
                header('Content-type: text/html; charset=UTF-8');
                echo json_encode(array('error' => 0, 'url' => $arr['url']),JSON_UNESCAPED_UNICODE);exit;
            }else{
                echo $is_exist;die;
            }
        }
        // 获取文件类型
        $file_type = get_file_type_for_ext($file->extension());
        // 附件大小限制
        $sizelimit = $this->request->post('sizelimit', -1);
        // $size_limit = $dir == 'images' ? sys_config('upload_image_size') : sys_config('upload_file_size');
        $size_limit = $file_type == 'image' ? sys_config('upload_image_size',0) : sys_config('upload_file_size',0);
        if (-1 != $sizelimit) {
            $sizelimit = intval($sizelimit);
            if ($sizelimit >= 0 && (0 == $size_limit || ($size_limit > 0 && $sizelimit > 0 && $size_limit > $sizelimit))) {
                $size_limit = $sizelimit;
            }
        }
        $size_limit = $size_limit * 1024;
        // 附件类型限制
        $extlimit = $this->request->post('extlimit', '');
        // $ext_limit = $dir == 'images' ? sys_config('upload_image_ext') : sys_config('upload_file_ext');
        $ext_limit = $file_type == 'image' ? sys_config('upload_image_ext') : sys_config('upload_file_ext');
        if($ext_limit != ''){
            $ext_limit_arr = parse_attr($ext_limit);
        }else{
            $ext_limit_arr = array();
        }
        if($extlimit != ''){
            $extlimit_arr = parse_attr($extlimit);
            $ext_limit = array_intersect($extlimit_arr,$ext_limit_arr);
        }else{
            $extlimit_arr = array();
            $ext_limit = $ext_limit_arr?:'';
        }
        // 判断附件大小是否超过限制
        if ($size_limit > 0 && ($file->getSize() > $size_limit)) {
            return json([
                'code'    => -1,
                'status'  => 0,
                'info'    => '附件过大',
                'state'   => '附件过大', // 兼容百度ueditor
                'error'   => 1,         // 兼容kindeditor
                "success" => 0,         // 兼容editormd
                'message' => '附件过大', // 兼容editormd和ueditor
            ]);
        }
        // 判断附件格式是否符合
        $file_ext = $file->extension();
        $error_msg = '';
        if ($ext_limit == '') {
            $error_msg = '获取文件后缀限制信息失败！';
        }
        try {
            $fileMine = $file->getMime();
        } catch (\Exception $ex) {
            $error_msg = $ex->getMessage();
        }
        if ($fileMine == 'text/x-php' || $fileMine == 'text/html') {
            $error_msg = '禁止上传非法文件！';
        }
        if ($error_msg == '' && (preg_grep("/php/i", $ext_limit))) {
            $error_msg = '禁止上传非法文件！';
        }
        if ($error_msg == '' && (!preg_grep("/$file_ext/i", $ext_limit))) {
            $error_msg = '附件类型不正确！';
        }
        if ($error_msg == '' && (!in_array($file_ext, $ext_limit))) {
            $error_msg = '附件类型不正确！';
        }
        if ($error_msg != '') {
            return json([
                'code'    => -1,
                'info'    => $error_msg,
                'state'   => $error_msg,// 兼容百度ueditor
                'error'   => 1,         // 兼容kindeditor
                "success" => 0,         // 兼容editormd
                'message' => $error_msg,// 兼容editormd和kindeditor
            ]);
        }
        // 附件上传钩子，用于第三方文件上传扩展
        /* if (sys_config('upload_driver','local') != 'local') {
            $hook_result = hook('UploadOssAfter', ['dir' => $dir, 'from' => $from, 'module' => $module, 'file' => $file], false);
            if (false !== $hook_result) {
                echo $hook_result;die;
            }
        } */
        // 移动到框架应用根目录指定目录下
        try {
            $savename = Filesystem::putFile($dir, $file, function ($file) {
                return date('Ymd') . '/' . md5((string) microtime(true));
            });
        } catch (\Exception $e) {
            return json([
                'code'    => -1,
                'info'    => $e->getMessage(),
                'state'   => $e->getMessage(), // 兼容百度ueditor
                'error'   => 1,                // 兼容kindeditor
                'success' => 0,                // 兼容editormd
                'message' => $e->getMessage(), // 兼容editormd和kindeditor
            ]);
        }
        $path = 'uploads/' . $savename;
        // 获取附件信息
        $file_info = [
            'aid'         => $this->admin_id,
            'uid'         => $this->user_id,
            'group_id'    => input('group_id',0),
            'file_name'   => basename($path),
            'file_type'   => $file_type,
            'name'        => $file->getOriginalName(),
            'module'      => $module,
            'path'        => $path,
            'thumb'       => '',
            'url'         => '',
            'mime'        => $file->getOriginalMime(),
            'ext'         => $file_ext,
            'size'        => $file->getSize(),
            'md5'         => $file->hash('md5'),
            'sha1'        => $file->hash('sha1'),
            'status'      => 1,
            'sort'        => 100,
            'create_time' => time(),
            'update_time' => time(),
        ];
        if($file_type == 'image'){
            try{
                // 水印功能
                $watermark = $this->request->post('watermark', '');//水印参数
                $watermark_arr = paramToArray($watermark);
                if(!$watermark_arr){
                    $watermark_arr = array();
                }
                $WEB_ENABLE_WATER = $watermark_arr['WEB_ENABLE_WATER']??sys_config('WEB_ENABLE_WATER');
                if($WEB_ENABLE_WATER){
                    $WEB_WATER_TYPE = $watermark_arr['WEB_WATER_TYPE']??sys_config('WEB_WATER_TYPE');
                    $WEB_WATER_IMG = $watermark_arr['WEB_WATER_IMG']??sys_config('WEB_WATER_IMG');
                    $WEB_WATER_IMG = get_file_path($WEB_WATER_IMG,2);
                    $WEB_WATER_TEXT = $watermark_arr['WEB_WATER_TEXT']??sys_config('WEB_WATER_TEXT');
                    $WEB_WATER_TEXT_SIZE = $watermark_arr['WEB_WATER_TEXT_SIZE']??sys_config('WEB_WATER_TEXT_SIZE');
                    $WEB_WATER_TEXT_COLOR = $watermark_arr['WEB_WATER_TEXT_COLOR']??sys_config('WEB_WATER_TEXT_COLOR');
                    $WEB_WATER_TMD = $watermark_arr['WEB_WATER_TMD']??sys_config('WEB_WATER_TMD');
                    $WEB_WATER_POS = $watermark_arr['WEB_WATER_POS']??sys_config('WEB_WATER_POS');
                    $res = ImageTool::water(STATIC_PATH . '/' . $path, $WEB_WATER_TYPE, $WEB_WATER_IMG, $WEB_WATER_TEXT, $WEB_WATER_TEXT_SIZE, $WEB_WATER_TEXT_COLOR, $WEB_WATER_TMD, $WEB_WATER_POS, true);
                    if ($res['code']) {
                        $path = $res['path'];
                        if(($is_exist = $this->is_exist(get_attachment_info(STATIC_PATH . '/' . $path)['md5'])) !== false){
                            @unlink(STATIC_PATH . '/' . $path);
                            if($from == 'kindeditor'){
                                $arr = json_decode($is_exist,true);
                                header('Content-type: text/html; charset=UTF-8');
                                echo json_encode(array('error' => 0, 'url' => $arr['url']),JSON_UNESCAPED_UNICODE);exit;
                            }else{
                                echo $is_exist;die;
                            }
                        }
                    } else {
                        return json([
                            'code'    => -1,
                            'info'    => $res['msg'],
                            'state'   => $res['msg'], // 兼容百度ueditor
                            'error'   => 1,           // 兼容kindeditor
                            'success' => 0,           // 兼容editormd
                            'message' => $res['msg'], // 兼容editormd和kindeditor
                        ]);
                    }
                }
                // 缩略功能
                $thumb = $this->request->post('thumb', '');//缩略参数
                $thumb_arr = paramToArray($thumb);
                if(!$thumb_arr){
                    $thumb_arr = array();
                }
                $WEB_ENABLE_THUMB = $thumb_arr['WEB_ENABLE_THUMB']??0;
                if($WEB_ENABLE_THUMB){
                    $WEB_THUMB_WIDTH = $thumb_arr['WEB_THUMB_WIDTH']??sys_config('WEB_THUMB_WIDTH');
                    $WEB_THUMB_HEIGHT = $thumb_arr['WEB_THUMB_HEIGHT']??sys_config('WEB_THUMB_HEIGHT');
                    $WEB_THUMB_TYPE = $thumb_arr['WEB_THUMB_TYPE']??sys_config('WEB_THUMB_TYPE');
                    $WEB_THUMB_SAVE_TYPE = $thumb_arr['WEB_THUMB_SAVE_TYPE']??sys_config('WEB_THUMB_SAVE_TYPE');
                    $WEB_THUMB_QUALITY = $thumb_arr['WEB_THUMB_QUALITY']??sys_config('WEB_THUMB_QUALITY');
                    $WEB_THUMB_INTERLACE = $thumb_arr['WEB_THUMB_INTERLACE']??sys_config('WEB_THUMB_INTERLACE');
                    $res = ImageTool::thumb(STATIC_PATH . '/' . $path, $WEB_THUMB_TYPE, $WEB_THUMB_WIDTH, $WEB_THUMB_HEIGHT, $WEB_THUMB_SAVE_TYPE, $WEB_THUMB_QUALITY, $WEB_THUMB_INTERLACE, true);
                    if ($res['code']) {
                        $path = $res['path'];
                        if(($is_exist = $this->is_exist(get_attachment_info(STATIC_PATH . '/' . $path)['md5'])) !== false){
                            @unlink(STATIC_PATH . '/' . $path);
                            if($from == 'kindeditor'){
                                $arr = json_decode($is_exist,true);
                                header('Content-type: text/html; charset=UTF-8');
                                echo json_encode(array('error' => 0, 'url' => $arr['url']),JSON_UNESCAPED_UNICODE);exit;
                            }else{
                                echo $is_exist;die;
                            }
                        }
                    } else {
                        return json([
                            'code'    => -1,
                            'info'    => $res['msg'],
                            'state'   => $res['msg'], // 兼容百度ueditor
                            'error'   => 1,           // 兼容kindeditor
                            'success' => 0,           // 兼容editormd
                            'message' => $res['msg'], // 兼容editormd和kindeditor
                        ]);
                    }
                }
                $file_info['file_name']   = basename($path);
                $file_info['path']        = $path;
                $file_info['mime']        = get_attachment_info(STATIC_PATH . '/' . $path)['mime'];
                $file_info['ext']         = get_attachment_info(STATIC_PATH . '/' . $path)['ext'];
                $file_info['size']        = get_attachment_info(STATIC_PATH . '/' . $path)['size'];
                $file_info['md5']         = get_attachment_info(STATIC_PATH . '/' . $path)['md5'];
                $file_info['sha1']        = get_attachment_info(STATIC_PATH . '/' . $path)['sha1'];
                $file_info['create_time'] = time();
                $file_info['update_time'] = time();
            }catch (\Exception $e) {
                return json([
                    'code'    => -1,
                    'info'    => '上传失败,'.$e->getMessage(),
                    'state'   => '上传失败,'.$e->getMessage(), // 兼容百度ueditor
                    'error'   => 1,                           // 兼容kindeditor
                    'success' => 0,                           // 兼容editormd
                    'message' => '上传失败,'.$e->getMessage(), // 兼容editormd和kindeditor
                ]);
            }
        }
        if (sys_config('upload_driver','local') != 'local') {
            $hook_result = hook('UploadOss', $file_info, false);
            // dump($hook_result);die;
            $arr = json_decode($hook_result,true);
            if($arr['code'] == 0){
                @unlink(STATIC_PATH . '/' . $path);
            }
            if($from == 'kindeditor'){
                header('Content-type: text/html; charset=UTF-8');
                echo json_encode(array('error' => 0, 'url' => $arr['url']),JSON_UNESCAPED_UNICODE);exit;
            }else{
                echo $hook_result;die;
            }
        }else{
            if ($file_add = Attachment_Model::create($file_info)) {
                $file_info['path'] = Config::get('view.tpl_replace_string.__STATIC__') . '/' . $file_info['path'];
                return json([
                    'code'    => 0,
                    'info'    => $file_info['name'] . '上传成功',
                    'id'      => $file_add['id'],
                    'path'    => $file_info['path'],
                    'state'   => "SUCCESS",          // 上传状态，上传成功时必须返回"SUCCESS" 兼容百度
                    'title'   => $file_info['name'], // 附件名，兼容百度
                    'url'     => $file_info['path'], // 返回的地址，兼容百度ueditor和editormd
                    'error'   => 0,                  // 兼容kindeditor
                    'success' => 1,                  // 兼容editormd
                    'message' => $file_info['name'], // 附件名，兼容editormd和kindeditor
                ]);
            } else {
                return json([
                    'code'    => -1,
                    'info'    => '上传成功,写入数据库失败',
                    'state'   => '上传成功,写入数据库失败', // 兼容百度ueditor
                    'error'   => 1,                       // 兼容kindeditor
                    'success' => 0,                       // 兼容editormd
                    'message' => '上传成功,写入数据库失败', // 兼容editormd和kindeditor
                ]);
            }
        }
    }
    private function ueditor()
    {
        $action = $this->request->get('action');
        switch ($action) {
            /* 获取配置信息 */
            case 'config':
                $result = $this->confing;
                break;
            /* 上传图片 */
            case 'uploadimage':
                return $this->saveFile('images', 'ueditor');
                break;
            /* 上传涂鸦 */
            case 'uploadscrawl':
                return $this->saveFile('images', 'ueditor_scrawl');
                break;
            /* 上传视频 */
            case 'uploadvideo':
                return $this->saveFile('videos', 'ueditor');
                break;
            /* 上传附件 */
            case 'uploadfile':
                return $this->saveFile('files', 'ueditor');
                break;
            /* 列出图片 */
            case 'listimage':
                return $this->showFileList('listimage');
                break;
            /* 列出附件 */
            case 'listfile':
                return $this->showFileList('listfile');
                break;
            /* 粘贴上传图片 */
            /* case 'catchimage':
                return $this->saveFile('images', 'source');
                break; */
            default:
                $result = array(
                    'state' => '请求地址出错',
                );
                break;
        }
        /* 输出结果 */
        if (isset($_GET["callback"])) {
            if (preg_match("/^[\w_]+$/", $_GET["callback"])) {
                return htmlspecialchars($_GET["callback"]) . '(' . $result . ')';
            } else {
                return json(['state' => 'callback参数不合法']);
            }
        } else {
            return json($result);
        }
    }
    private function kindeditor()
    {
        $action = $this->request->get('action');
        switch ($action) {
            /* 上传 */
            case 'upload':
                // return $this->kindeditor_upload();
                return $this->saveFile('images', 'kindeditor');
                break;
            /* 空间 */
            case 'manager':
                // return $this->kindeditor_file_manager();
                return $this->showKindeditorFileList();
                break;
            default:
                $result = array(
                    'state' => '请求地址出错',
                );
                break;
        }
    }
    protected function kindeditor_upload()
    {
        $php_url = dirname($_SERVER['PHP_SELF']) . '/';
        // 文件保存目录路径
        $save_path = STATIC_PATH . '/uploads/kindeditor/';
        // 文件保存目录URL
        $save_url = Config::get('view.tpl_replace_string.__STATIC__') . '/uploads/kindeditor/';
        //定义允许上传的文件扩展名
        $ext_arr = array(
            'image' => array('gif', 'jpg', 'jpeg', 'png', 'bmp'),
            'flash' => array('swf', 'flv'),
            // 'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'asf', 'rm', 'rmvb'),
            'media' => array('swf', 'flv', 'mp3', 'wav', 'wma', 'wmv', 'mid', 'avi', 'mpg', 'asf', 'rm', 'rmvb','mp4'),
            'file' => array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'txt', 'zip', 'rar', 'gz', 'bz2'),
        );
        //最大文件大小
        // $max_size = 1000000;
        $max_size = 30000000;// 30M吧
        $save_path = realpath($save_path) . '/';
        //PHP上传失败
        if (!empty($_FILES['imgFile']['error'])) {
            switch($_FILES['imgFile']['error']){
                case '1':
                    $error = '超过php.ini允许的大小。';
                    break;
                case '2':
                    $error = '超过表单允许的大小。';
                    break;
                case '3':
                    $error = '图片只有部分被上传。';
                    break;
                case '4':
                    $error = '请选择图片。';
                    break;
                case '6':
                    $error = '找不到临时目录。';
                    break;
                case '7':
                    $error = '写文件到硬盘出错。';
                    break;
                case '8':
                    $error = 'File upload stopped by extension。';
                    break;
                case '999':
                default:
                    $error = '未知错误。';
            }
            alert($error);
        }
        //有上传文件时
        if (empty($_FILES) === false) {
            //原文件名
            $file_name = $_FILES['imgFile']['name'];
            //服务器上临时文件名
            $tmp_name = $_FILES['imgFile']['tmp_name'];
            //文件大小
            $file_size = $_FILES['imgFile']['size'];
            //检查文件名
            if (!$file_name) {
                alert("请选择文件。");
            }
            //检查目录
            if (@is_dir($save_path) === false) {
                alert("上传目录不存在。");
            }
            //检查目录写权限
            if (@is_writable($save_path) === false) {
                alert("上传目录没有写权限。");
            }
            //检查是否已上传
            if (@is_uploaded_file($tmp_name) === false) {
                alert("上传失败。");
            }
            //检查文件大小
            if ($file_size > $max_size) {
                alert("上传文件大小超过限制。");
            }
            //检查目录名
            $dir_name = empty($_GET['dir']) ? 'image' : trim($_GET['dir']);
            if (empty($ext_arr[$dir_name])) {
                alert("目录名不正确。");
            }
            //获得文件扩展名
            $temp_arr = explode(".", $file_name);
            $file_ext = array_pop($temp_arr);
            $file_ext = trim($file_ext);
            $file_ext = strtolower($file_ext);
            //检查扩展名
            if (in_array($file_ext, $ext_arr[$dir_name]) === false) {
                alert("上传文件扩展名是不允许的扩展名。\n只允许" . implode(",", $ext_arr[$dir_name]) . "格式。");
            }
            //创建文件夹
            if ($dir_name !== '') {
                $save_path .= $dir_name . "/";
                $save_url .= $dir_name . "/";
                if (!file_exists($save_path)) {
                    mkdir($save_path);
                }
            }
            $ymd = date("Ymd");
            $save_path .= $ymd . "/";
            $save_url .= $ymd . "/";
            if (!file_exists($save_path)) {
                mkdir($save_path);
            }
            //新文件名
            $new_file_name = date("YmdHis") . '_' . rand(10000, 99999) . '.' . $file_ext;
            //移动文件
            $file_path = $save_path . $new_file_name;
            if (move_uploaded_file($tmp_name, $file_path) === false) {
                alert("上传文件失败。");
            }
            @chmod($file_path, 0644);
            $file_url = $save_url . $new_file_name;
            header('Content-type: text/html; charset=UTF-8');
            echo json_encode(array('error' => 0, 'url' => $file_url),JSON_UNESCAPED_UNICODE);exit;
        }
    }
    protected function kindeditor_file_manager()
    {
        //根目录路径，可以指定绝对路径
        $root_path = STATIC_PATH . '/uploads/kindeditor/';
        // 根目录URL，可以指定绝对路径，比如 http://www.yoursite.com/attached/
        $root_url = Config::get('view.tpl_replace_string.__STATIC__') . '/uploads/kindeditor/';
        //图片扩展名
        $ext_arr = array('gif', 'jpg', 'jpeg', 'png', 'bmp');
        //目录名
        $dir_name = empty($_GET['dir']) ? '' : trim($_GET['dir']);
        if (!in_array($dir_name, array('', 'image', 'flash', 'media', 'file'))) {
            echo "Invalid Directory name.";
            exit;
        }
        if ($dir_name !== '') {
            $root_path .= $dir_name . "/";
            $root_url .= $dir_name . "/";
            if (!file_exists($root_path)) {
                mkdir($root_path);
            }
        }
        //根据path参数，设置各路径和URL
        if (empty($_GET['path'])) {
            $current_path = realpath($root_path) . '/';
            $current_url = $root_url;
            $current_dir_path = '';
            $moveup_dir_path = '';
        } else {
            $current_path = realpath($root_path) . '/' . $_GET['path'];
            $current_url = $root_url . $_GET['path'];
            $current_dir_path = $_GET['path'];
            $moveup_dir_path = preg_replace('/(.*?)[^\/]+\/$/', '$1', $current_dir_path);
        }
        //echo realpath($root_path);
        //排序形式，name or size or type
        $order = empty($_GET['order']) ? 'name' : strtolower($_GET['order']);
        //不允许使用..移动到上一级目录
        if (preg_match('/\.\./', $current_path)) {
            echo 'Access is not allowed.';
            exit;
        }
        //最后一个字符不是/
        if (!preg_match('/\/$/', $current_path)) {
            echo 'Parameter is not valid.';
            exit;
        }
        //目录不存在或不是目录
        if (!file_exists($current_path) || !is_dir($current_path)) {
            echo 'Directory does not exist.';
            exit;
        }
        //遍历目录取得文件信息
        $file_list = array();
        if ($handle = opendir($current_path)) {
            $i = 0;
            while (false !== ($filename = readdir($handle))) {
                if ($filename{0} == '.') continue;
                $file = $current_path . $filename;
                if (is_dir($file)) {
                    $file_list[$i]['is_dir'] = true; //是否文件夹
                    $file_list[$i]['has_file'] = (count(scandir($file)) > 2); //文件夹是否包含文件
                    $file_list[$i]['filesize'] = 0; //文件大小
                    $file_list[$i]['is_photo'] = false; //是否图片
                    $file_list[$i]['filetype'] = ''; //文件类别，用扩展名判断
                } else {
                    $file_list[$i]['is_dir'] = false;
                    $file_list[$i]['has_file'] = false;
                    $file_list[$i]['filesize'] = filesize($file);
                    $file_list[$i]['dir_path'] = '';
                    $file_ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    $file_list[$i]['is_photo'] = in_array($file_ext, $ext_arr);
                    $file_list[$i]['filetype'] = $file_ext;
                }
                $file_list[$i]['filename'] = $filename; //文件名，包含扩展名
                $file_list[$i]['datetime'] = date('Y-m-d H:i:s', filemtime($file)); //文件最后修改时间
                $i++;
            }
            closedir($handle);
        }
        usort($file_list, 'cmp_func');
        $result = array();
        //相对于根目录的上一级目录
        $result['moveup_dir_path'] = $moveup_dir_path;
        //相对于根目录的当前目录
        $result['current_dir_path'] = $current_dir_path;
        //当前目录的URL
        $result['current_url'] = $current_url;
        //文件数
        $result['total_count'] = count($file_list);
        //文件列表数组
        $result['file_list'] = $file_list;
        // 输出JSON字符串
        // header('Content-type: application/json; charset=UTF-8');
        // $json = new Services_JSON();
        // echo $json->encode($result);
        return json($result);
    }
    protected function showKindeditorFileList()
    {
        $size      = input('get.size/d', 0);
        $start     = input('get.start/d', 0);
        if ($size == 0) {
            $size = 100;
        }
        $dir = $this->request->get('dir');
        $data['current_dir_path'] = '';
        $data['current_url'] = '';
        $data['moveup_dir_path'] = '';
        if($dir == 'image'){
            $file_type = 'image';
        }elseif($dir == 'file'){
            $file_type = 'file';
        }elseif($dir == 'media'){
            $file_type = 'video';
        }else{
            $file_type = 'image';
        }
        $filelist = Attachment_Model::order('id desc')
        ->field(true)
        ->where('file_type', '=', $file_type)
        ->where('status', 1)
        ->limit($start, $size)
        ->select()->toArray();
        $data['file_list'] = array();
        if($filelist){
            foreach($filelist as $k=>$v){
                $arr['datetime'] = $v['create_time'];
                $arr['dir_path'] = '';
                if($v['driver'] == 'local'){
                    $arr['filename'] = Config::get('view.tpl_replace_string.__STATIC__') . '/' . $v['path'];
                }else{
                    $arr['filename'] = $v['path'];
                }
                $arr['filesize'] = $v['size'];
                $arr['filetype'] = $v['ext'];
                $arr['has_file'] = false;
                $arr['is_dir']   = false;
                $arr['is_photo'] = true;
                $data['file_list'][] = $arr;
            }
        }
        return json($data);
    }
    /**
     * @param string $type 类型
     * @return \think\response\Json
     */
    protected function showFileList($type = '')
    {
        /* 获取参数 */
        $size      = input('get.size/d', 0);
        $start     = input('get.start/d', 0);
        $allowExit = input('get.exit', '');
        if ($size == 0) {
            $size = 20;
        }
        /* 判断类型 */
        switch ($type) {
            /* 列出附件 */
            case 'listfile':
                $allowExit = '' == $allowExit ? sys_config('upload_file_ext') : $allowExit;
                break;
            /* 列出图片 */
            case 'listimage':
            default:
                $allowExit = '' == $allowExit ? sys_config('upload_image_ext') : $allowExit;
        }
        /* 获取附件列表 */
        $filelist = Attachment_Model::order('id desc')
        ->where('ext', 'in', $allowExit)
        ->where('status', 1)
        ->limit($start, $size)
        ->column('id,path,driver,create_time,name,size');
        if (empty($filelist)) {
            return json(array(
                "state" => "没有找到附件",
                "list"  => [],
                "start" => $start,
                "total" => 0
            ));
        }
        $list      = [];
        $i         = 0;
        foreach ($filelist as $value) {
            $list[$i]['id']    = $value['id'];
            if($value['driver'] == 'local'){
                $value['path'] = Config::get('view.tpl_replace_string.__STATIC__') . '/' . $value['path'];
            }
            $list[$i]['url']   = $value['path'];
            $list[$i]['name']  = $value['name'];
            $list[$i]['size']  = format_bytes($value['size']);
            $list[$i]['mtime'] = $value['create_time'];
            $i++;
        }
        /* 返回数据 */
        $result = array(
            "state" => "SUCCESS",
            "list"  => $list,
            "start" => $start,
            "total" => Attachment_Model::where('ext', 'in', $allowExit)->count(),
        );
        return json($result);
    }
    // 获取会员组
    protected function get_member_group()
    {
        $data = Db::name('member_group')->select();
        if ($data) {
            $data = $data->toArray();
        } else {
            return;
        }
        $return = array();
        foreach ($data as $k => $v) {
            if ($v['expand']) {
                $v['expand'] = unserialize($v['expand']);
            } else {
                $v['expand'] = array();
            }
            $return[$v['id']] = $v;
        }
        return $return;
    }
    // 判断附件是否存在
    protected function is_exist($md5)
    {
        if ($file_exists = Attachment_Model::where('md5',$md5)->find()) {
            if($file_exists['driver'] == 'local'){
                $file_exists['path'] = Config::get('view.tpl_replace_string.__STATIC__') . '/' . $file_exists['path'];
            }
            return json_encode([
                'code'    => 0,
                'info'    => $file_exists['name'] . '上传成功',
                'id'      => $file_exists['id'],
                'path'    => $file_exists['path'],
                "state"   => "SUCCESS",            // 上传状态，上传成功时必须返回"SUCCESS" 兼容百度
                "title"   => $file_exists['name'], // 附件名，兼容百度
                "url"     => $file_exists['path'], // 返回的地址，兼容百度ueditor和editormd
                "error"   => 0,                    // 兼容kindeditor
                "success" => 1,                    // 兼容editormd
                "message" => $file_exists['name'], // 附件名，兼容editormd和kindeditor
            ],JSON_UNESCAPED_UNICODE);
        } else {
            return false;
        }
    }
    // 上传大文件
    protected function upload_max_file()
    {
        if(!is_super_administrator(is_login())){
            echo '非超级管理员不允许上传！';die;
        }
        $dir = $this->request->get('dir');
        $dir = 'max_file';
        $module = $this->request->get('module');
        // 大文件切割上传，把每次上传的数据合并成一个文件
        $filepath = STATIC_PATH . '/uploads/' . $dir . '/' . $_POST['newfilename'];
        $filepath = iconv("UTF-8", "gb2312", $filepath);
        mk_dirs(dirname($filepath));
        // 第一次上传时没有文件，就创建文件，此后上传只需要把数据追加到此文件中
        if (!file_exists($filepath)) {
            move_uploaded_file($_FILES['file']['tmp_name'], $filepath);
        } else {
            file_put_contents($filepath, file_get_contents($_FILES['file']['tmp_name']), FILE_APPEND);
        }
        $totalsize = input('totalsize');
        $end = input('end');
        if($totalsize <= $end){
            $ext = input('ext');
            $mime = mime_content_type($filepath);
            $md5  = hash_file('md5', $filepath);
            $sha1 = hash_file('sha1', $filepath);
            $size = filesize($filepath);
            // 获取文件类型
            $file_type = get_file_type_for_ext($ext);
            if($file_exists = Attachment_Model::where('md5',$md5)->find()){
                @unlink($filepath);
                echo '文件'. $file_exists['name'] .'已存在！';die;
            }
            // 获取附件信息
            $file_info = [
                'aid'         => $this->admin_id,
                'uid'         => $this->user_id,
                'group_id'    => input('group_id',0),
                'file_name'   => basename($filepath),
                'file_type'   => $file_type,
                'name'        => input('filename'),
                'module'      => $module,
                'path'        => str_replace(STATIC_PATH . '/', '', $filepath),
                'thumb'       => '',
                'url'         => '',
                'mime'        => $mime,
                'ext'         => $ext,
                'size'        => $size,
                'md5'         => $md5,
                'sha1'        => $sha1,
                'status'      => 1,
                'sort'        => 100,
                'create_time' => time(),
                'update_time' => time(),
            ];
            if (Attachment_Model::create($file_info)) {
                echo 1;die;
            }else{
                @unlink($filepath);
                echo '上传失败！';
            }
        }else{
            echo 1;die;
        }
    }
}
