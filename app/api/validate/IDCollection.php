<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | ids字符串验证器
// +----------------------------------------------------------------------
namespace app\api\validate;

class IDCollection extends BaseValidate
{
    protected $rule = [
        'ids' => 'require|checkIDs'
    ];
    // 属性定义错误提示信息
    protected $message = [
        'ids' => 'ids参数必须为以逗号分隔的多个正整数,仔细看文档啊'
    ];
    /**
     * 自定义ids字符串的验证规则
     * 自定义验证规则：
     * 验证方法可以传入的参数共有5个（后面三个根据情况选用），依次为：
     * 1、验证数据
     * 2、验证规则
     * 3、全部数据（数组）
     * 4、字段名
     * 5、字段描述
     * 自定义的验证规则方法名不能和已有的规则冲突。
     * @param  [type]  $value [验证数据]
     * @param  string  $rule  [验证规则]
     * @param  string  $data  [全部数据（数组）]
     * @param  string  $field [字段名]
     * @param  string  $title [字段描述]
     * @return boolean        [description]
     */
    protected function checkIDs($value, $rule, $data = [], $field = '', $title = '')
    {
        $values = explode(',', $value);
        if (empty($values)) {
            return false;
        }
        foreach ($values as $id) {
            //判断是否正整数
            if (!$this->isPositiveInteger($id, $rule, $data = [], $field = '', $title = '')) {
                // 必须是正整数
                return false;
            }
        }
        return true;
    }
    /**
     * 自定义ids字符串的验证规则
     * 自定义验证规则：
     * 验证方法可以传入的参数共有5个（后面三个根据情况选用），依次为：
     * 1、验证数据
     * 2、验证规则
     * 3、全部数据（数组）
     * 4、字段名
     * 5、字段描述
     * 自定义的验证规则方法名不能和已有的规则冲突。
     * @param  [type]  $value [验证数据]
     * @param  string  $rule  [验证规则]
     * @param  string  $data  [全部数据（数组）]
     * @param  string  $field [字段名]
     * @param  string  $title [字段描述]
     * @return boolean        [description]
     */
    protected function checkIDs1($value, $rule, $data = [], $field = '', $title = '')
    {
        $result = true;
        $values = explode(',', $value);
        if (empty($values)) {
            $result = false;
        }
        // array_walk() 函数对数组中的每个元素应用用户自定义函数。在函数中，数组的键名和键值是参数。
        array_walk($values, function ($id) use (&$result) {
            if (!$this->isPositiveInteger($id, $rule, $data = [], $field = '', $title = '')) {
                // 必须是正整数
                $result = false;
            }
        });
        return $result;
    }
}
