<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | OpensslAES加密、解密扩展类
// +----------------------------------------------------------------------
namespace app\api\lib;

class OpensslAES
{
    // 密码学方式。openssl_get_cipher_methods() 可获取有效密码方式列表。
    protected $method = 'AES-128-ECB';
    // key，aes密钥，服务端和客户端必须保持一致
    protected $secret_key = 'sgg45747ss223455';
    // 非 NULL 的初始化向量。
    protected $iv = '';
    // options 是以下标记的按位或： OPENSSL_RAW_DATA 、 OPENSSL_ZERO_PADDING。
    protected $options = 0;
    /**
     * 构造函数
     * @param string  $key     [key]
     * @param string  $method  [密码学方式。openssl_get_cipher_methods() 可获取有效密码方式列表。]
     * @param string  $iv      [非 NULL 的初始化向量。]
     * @param integer $options [options 是以下标记的按位或： OPENSSL_RAW_DATA 、 OPENSSL_ZERO_PADDING。]
     */
    public function __construct($key = '', $method = '', $iv = '', $options = 0)
    {
        if ($key) {
            $this->secret_key = $key;
        }
        if ($method) {
            $this->method = $method;
        }
        if ($iv) {
            $this->iv = $iv;
        }
        if ($options) {
            $this->options = $options;
        }
    }
    /**
     * 加密
     * @param  string $input [待加密的明文信息数据。]
     * @return [type]        [description]
     */
    public function encrypt($input = '')
    {
        return openssl_encrypt($input, $this->method, $this->secret_key, $this->options, $this->iv);
    }
    /**
     * 解密
     * @param  [type] $sStr [将被解密的密文。]
     * @return [type]       [description]
     */
    public function decrypt($sStr)
    {
        return openssl_decrypt($sStr, $this->method, $this->secret_key, $this->options, $this->iv);
    }
}
