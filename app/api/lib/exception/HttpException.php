<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 自定义手动抛出异常处理类(Http)
// +----------------------------------------------------------------------
namespace app\api\lib\exception;

class HttpException extends \RuntimeException
{
    // http状态码
    private $httpCode;
    // headers信息
    private $headers;
    /**
     * 构造方法
     * @param integer         $code      [异常代码]
     * @param [type]          $message   [异常消息内容]
     * @param [type]          $httpCode  [http状态码]
     * @param \Exception|null $previous  [description]
     * @param array           $headers   [headers头部信息]
     */
    public function __construct($code = 10404, $message = 'global:your required resource are not found', $httpCode = 404, \Exception $previous = null, array $headers = [])
    {
        // http状态码
        $this->httpCode = $httpCode;
        // headers信息
        $this->headers  = $headers;
        // 重写覆盖了
        parent::__construct($message, $code, $previous);
    }
    // 获取http状态码
    public function getHttpCode()
    {
        return $this->httpCode;
    }
    // 获取headers信息
    public function getHeaders()
    {
        return $this->headers;
    }
}
