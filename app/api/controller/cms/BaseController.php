<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 自定义基础控制器
// +----------------------------------------------------------------------
namespace app\api\controller;

use app\api\lib\DataHandle;
use app\api\service\JwtToken;
use app\BaseController as Controller;

class BaseController extends Controller
{
    use DataHandle;
    public $token;
    // 初始化的方法
    protected function initialize()
    {
        // 方法一：
        // 检查时间戳
        // $this->checkTimestamp();
        // 检查签名
        // $this->checkSign();
        // 方法二：
        // 检查签名和检查每次请求的数据是否合法
        $this->checkRequestAuth();
        // 检查API接口请求频率
        $this->checkRequestFrequency();
        // 获取token
        $this->token = request()->header('token');
        parent::initialize();
    }
    // 检查签名
    protected function checkRequestAuth()
    {
        $this->checkSignature();
    }
    // 检查API接口请求频率
    protected function checkRequestFrequency()
    {
        $this->checkFrequency();
    }
    // 检查权限
    protected function checkPermission()
    {
        JwtToken::needPermission($this->token);
    }
}
