<?php

return array (
  'autoload' => false,
  'hooks' => 
  array (
    'UploadOssAfter' => 
    array (
      0 => 'aliyunoss',
      1 => 'qiniuoss',
      2 => 'tencentoss',
    ),
    'UploadOss' => 
    array (
      0 => 'aliyunoss',
      1 => 'qiniuoss',
      2 => 'tencentoss',
    ),
    'DeleteOss' => 
    array (
      0 => 'aliyunoss',
      1 => 'qiniuoss',
      2 => 'tencentoss',
    ),
    'Testhook' => 
    array (
      0 => 'backup',
      1 => 'member',
    ),
    'Baidupush' => 
    array (
      0 => 'baidupush',
    ),
    'EmsSend' => 
    array (
      0 => 'easyemail',
    ),
    'EmsNotice' => 
    array (
      0 => 'easyemail',
    ),
    'EmsCheck' => 
    array (
      0 => 'easyemail',
    ),
    'SmsSend' => 
    array (
      0 => 'easysms',
    ),
    'SmsNotice' => 
    array (
      0 => 'easysms',
    ),
    'SmsCheck' => 
    array (
      0 => 'easysms',
    ),
    'UserSidenavAfter' => 
    array (
      0 => 'pay',
    ),
    'PageFooter' => 
    array (
      0 => 'returntop',
    ),
    'Stats' => 
    array (
      0 => 'stats',
    ),
    'ClearStats' => 
    array (
      0 => 'stats',
    ),
    'ThirdLogin' => 
    array (
      0 => 'thirdlogin',
    ),
    'SyncLogin' => 
    array (
      0 => 'thirdlogin',
    ),
    'UserConfig' => 
    array (
      0 => 'thirdlogin',
    ),
  ),
  'route' => 
  array (
  ),
  'service' => 
  array (
  ),
);